package com.dheeraj.FirstSpring;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Component;
@Component
@Primary
public class Cat implements Pet {
	@Value("pussy cat")
	String name;
	
	public void activity() {
		System.out.println(name);
		System.out.println("meoww");
	}

}
