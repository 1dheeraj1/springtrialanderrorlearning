package com.dheeraj.FirstSpring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
//@ComponentScan(basePackageClasses =Dog.class ) //dog class is present in FirstSpring pakage, so it will scan all classes in that package
//@ComponentScan(basePackages ="com/dheeraj/FirstSpring")// either /
@ComponentScan(basePackages ="com.dheeraj.FirstSpring")// or dot
public class AppConfig {
	
}
