package com.dheeraj.FirstSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
@Component
public class Student implements Person 
{
	@Value("Dheeraj")
	private String name;
	@Value("25")
	private int age;
	@Autowired
	private Pet pet;
	public void personDetails() {
		System.out.printf("name is %s, age is %d, pet details are:\n",name,age);
		pet.activity();
	}
	
}
