package com.dheeraj.FirstSpring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Component;
@Component
public class Teacher implements Person 
{
	@Value("Ram")
	private String name;
	@Value("50")
	private int age;
	@Autowired
	private Pet pet;

	public void personDetails() {
		System.out.printf("name is %s, age is %d, pet details are:\n",name,age);
		pet.activity();
	}
	
}
