package com.dheeraj.FirstSpring;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Component;
@Component
public class Dog implements Pet {
	@Value("Rocky")
	String name;
	public void activity() {
		System.out.println(name);
		System.out.println("barks");
	}

}
