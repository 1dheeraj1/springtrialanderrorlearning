package com.dheeraj.FirstSpring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App
{
    public static void main( String[] args)
    {
    	ApplicationContext context= new AnnotationConfigApplicationContext(AppConfig.class);
    	Person p=context.getBean("teacher",Person.class);//searches for a method named teacher in AppConfig class
    	p.personDetails();
    }
}

